const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

function disp(a,b){
    let nLine = document.createElement("div")
    nLine.innerHTML = "<h4>kata (" + a + ")</h4>" + b
    document.body.appendChild(nLine)
}

function kata1(){

    let b = ""
    for (let i=1; i<=25; i++){
        b += i + ","
    }
    return b 
}
disp(1, kata1())

function kata2(){
    let b = ""
    for (let i=25; i>=1; i--){
        b += i + ","
    }
    return b 
}
disp(2, kata2())

function kata3(){
    let a = ""
    let b = ""
    for (let i=-1; i>=-25; i--){
        b += i + ","
    }
    return b 
}
disp(3, kata3())

function kata4(){
    let a = ""
    let b = ""
    for (let i =-25; i <=-1; i++){
        b += i + ","
    }
    return b 
}
disp(4, kata4())

function kata5(){
    let b = ""
    for (let i=25; i >= -25; i-=2){
        b += i + ","
    }
    return b 
}
disp(5, kata5())

function kata6(){

    let b = ""
    for (let i=3; i<=100; i+=3){
        b += i + ","
    }
    return b 
}
disp(6, kata6())

function kata7(){

    let b = ""
    for (let i=7; i<=100; i+=7){
        b += i + ","
    }
    return b 
}
disp(7, kata7())

function kata8(){
    let b = ""
    for (let i=100; i>=1; i--){
        if (i%3 === 0 || i%7 === 0){
            b += i + ","
        }
        
    }
    return b 
}
disp(8, kata8())

function kata9(){

    let b = ""
    for (let i=5; i<=100; i++){
        if (i%2 === 1 && i%5 === 0){
         
            b += i + ","

        }
        
    }
    return b 
}
disp(9, kata9())

function kata10(){ 
   
    let b = ""
    for (let i=0; i<sampleArray.length; i++){
       
        b += sampleArray[i] + " , "
    }

    return b
}
disp(10, kata10())

function kata11(){ 
   
    let b = ""
    for (let i=0; i<sampleArray.length; i++){
       if (sampleArray[i]%2===0){
        b += sampleArray[i] + " , "
    }
    }
    return b
}
disp(11, kata11())

function kata12(){ 
   
    let b = ""
    for (let i=0; i<sampleArray.length; i++){
       if (sampleArray[i]%2===1){
        b += sampleArray[i] + " , "
    }
    }
    return b
}
disp(12, kata12())

function kata13(){ 
   
    let b = ""
    for (let i=0; i<sampleArray.length; i++){
       if (sampleArray[i]%8===0){
        b += sampleArray[i] + " , "
    }
    }
    return b
}
disp(13, kata13())

function kata14(){ 
   
    let b = ""
    for (let i=0; i<sampleArray.length; i++){
       
        b += Math.pow(sampleArray[i], 2) + " , "
    }

    return b
}
disp(14, kata14())

function kata15(){

    let b = 0

    for (let i=1; i<=20; i++){
    
        b += i 
    }
    return b 
}
disp(15, kata15())

function kata16(){

    let b = 0

    for (let i=0; i<sampleArray.length; i++){
    
        b += sampleArray[i]
    }
    return b 
}
disp(16, kata16())


function kata17(){

    let b = 0

    for (let i=0; i<sampleArray.length; i++){
    
        b = (Math.min(...sampleArray))
    }
    return b 
}
disp(17, kata17())

function kata18(){

    let b = 0

    for (let i=0; i<sampleArray.length; i++){
    
        b = (Math.max(...sampleArray))
    }
    return b 
}
disp(18, kata18())



//  Worked with a study group to get an understanding of what was needed.  Did the bulk of work myself.  Special shout out to Steven Carrington.

